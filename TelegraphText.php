<?php
interface LoggerInterface {
    public function logMessage($errorText);
    public function lastMessages($errorCnt);
}

interface EventListenerInterface {
    public function attachEvent($methodName, $callbackName);
    public function detouchEvent($methodName);
}

trait logger {
    public function logMessage($errorText) {}
    
    public function lastMessages($errorCnt) {}
}

trait eventListener {
    public function attachEvent($methodName, $callbackName) {}
    
    public function detouchEvent($methodName) {}
}

abstract class Storage implements LoggerInterface, EventListenerInterface {
    use logger, eventListener;
    
    abstract function create(Object $obj);
    abstract function read($id = '', $slug = '');
    abstract function update($id = '', $slug = '', Object $newObj);
    abstract function delete($id = '', $slug = '');
    abstract function list();
}

abstract class View {
    public $storage;
    
    public function __construct(Storage $storage) {
        $this -> storage = $storage;
    }
    
    abstract function displayTextById($id);
    abstract function displayTextByUrl($url);
}

abstract class User implements EventListenerInterface{
    use eventListener;
    
    public $id;
    public $name;
    public $role;
    
    abstract function getTextsToEdit();
}

class FileStorage extends Storage {
    public function create(Object $obj)
    {
        $fileName = substr($obj -> slug, 0, strpos($obj -> slug, '.'));
        $exp = substr($obj -> slug, strpos($obj -> slug, '.'));
        $newFileName = $fileName . '_' . date('d_m_Y');
        
        $cnt = 1;
        
        while (file_exists($newFileName.$exp)) {
            $newFileName = $fileName . '_' . date('d_m_Y') . "_$cnt";
            $cnt++;
        }
        
        $obj -> slug = $newFileName.$exp;
        
        file_put_contents($newFileName.$exp, serialize($obj));
        
        return $obj -> slug;
    }
    
    public function read($id = '', $slug = '')
    {
        if (!empty($id)) {
            return false;
        }
        elseif (!empty($slug) && file_exists($slug)) {
            return unserialize(file_get_contents($slug));
        }
        
        return false;
    }
    
    public function update($id = '', $slug = '', Object $newObj)
    {
        if (!empty($slug) && file_exists($slug)) {
            file_put_contents($slug, serialize($newObj));
        }
    }
    
    public function delete($id = '', $slug = '')
    {
        if (!empty($slug) && file_exists($slug)) {
            unlink($slug);
        }
    }
    
    public function list()
    {
        $arPaths = scandir(__DIR__);
        $arObj = [];
        foreach ($arPaths as $path) {
            if (is_file($path)) {
                $arObj[] = unserialize(file_get_contents($path));
            }
        }
        
        return $arObj;
    }
}
        
class TelegraphText {
    public $title, $text, $author, $published, $slug, $fileStorage;
    
    public function __construct($authorName, $fileName, FileStorage $fileStorage) {
        $this -> author = $authorName;
        $this -> slug = $fileName;
        $this -> published = date('d.m.Y H:i:s');
        $this -> fileStorage = $fileStorage;
    }
    
    public function storeText() 
    {
        $arResult = [
            'title' => $this -> title,
            'text' => $this -> text,
            'author' => $this -> author,
            'published' => $this -> published,
            ];
        
        $this -> fileStorage -> create($this);
    }
    
    public function loadText() 
    {
        
        if (file_exists($this -> slug)) {
            $arResult = $this -> fileStorage -> read('', $this -> slug);
            
            if (!empty($arResult)) {
                
                $this -> title = $arResult ->title;
                $this -> text = $arResult -> text;
                $this -> author = $arResult -> author;
                $this -> published = $arResult -> published;
                
                return $this -> text;
            }
        }
        
        return false;
    }
    
    public function editText($title, $text) 
    {
        $this -> title = $title;
        $this -> text = $text;
    }
}

$test = new TelegraphText('J. Smith', 'test.txt', new FileStorage());

$test ->editText('Заголовок', 'Какой-то очень небольшой текст 3');

$test -> storeText();

echo $test -> loadText();